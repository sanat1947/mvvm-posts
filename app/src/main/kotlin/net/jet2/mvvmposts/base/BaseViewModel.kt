package net.jet2.mvvmposts.base

import androidx.lifecycle.ViewModel
import net.jet2.mvvmposts.injection.component.DaggerViewModelInjector
import net.jet2.mvvmposts.injection.component.ViewModelInjector
import net.jet2.mvvmposts.injection.module.NetworkModule
import net.jet2.mvvmposts.ui.post.PostListViewModel
import net.jet2.mvvmposts.ui.post.PostViewModel

abstract class BaseViewModel:ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is PostListViewModel -> injector.inject(this)
            is PostViewModel -> injector.inject(this)
        }
    }
}