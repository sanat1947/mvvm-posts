package net.jet2.mvvmposts.network

import io.reactivex.Observable
import net.jet2.mvvmposts.model.Post
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * The interface which provides methods to get result of webservices
 */
interface PostApi {
    /**
     * Get the list of the pots from the API
     */
    @GET("blogs")
    fun getPosts(@Query("page") page:Int, @Query("limit") limit:Int): Observable<List<Post>>
}