package net.jet2.mvvmposts.ui.post

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import com.squareup.picasso.Picasso
import net.jet2.mvvmposts.base.BaseViewModel
import net.jet2.mvvmposts.model.Post


class PostViewModel:BaseViewModel() {

    private val userImage = MutableLiveData<String>()
    private val userName = MutableLiveData<String>()
    private val userDesignation = MutableLiveData<String>()
    private val postTime = MutableLiveData<String>()
    private val postImage = MutableLiveData<String>()
    private val postContent = MutableLiveData<String>()
    private val postTitle = MutableLiveData<String>()
    private val postUrl = MutableLiveData<String>()
    private val postLikes = MutableLiveData<String>()
    private val postComments = MutableLiveData<String>()
    private val picasso = Picasso.get()

    fun bind(post: Post){
        userImage.value = post.user[0].avatar
        userName.value = post.user[0].name + " "+post.user[0].lastname
        userDesignation.value = post.user[0].designation
        postImage.value = post.media[0].image
        postContent.value = post.content
        postTitle.value = post.media[0].title
        postUrl.value = post.media[0].url
        postLikes.value = post.likes.toString() +" Likes"
        postComments.value = post.comments.toString() + " Comments"
    }

    fun getUserImage():MutableLiveData<String>{
        return userImage
    }

    fun getUserName():MutableLiveData<String>{
        return userName
    }

    fun getUserDesignation():MutableLiveData<String>{
        return userDesignation
    }

    fun getPostImage():MutableLiveData<String>{
        return postImage
    }

    fun getPostContent():MutableLiveData<String>{
        return postContent
    }

    fun getPostTitle():MutableLiveData<String>{
        return postTitle
    }

    fun getPostUrl():MutableLiveData<String>{
        return postUrl
    }

    fun getPostLikes():MutableLiveData<String>{
        return postLikes
    }

    fun getPostComments():MutableLiveData<String>{
        return postComments
    }

    fun getPicasso():Picasso{
        return picasso
    }

    companion object {
        @BindingAdapter("imageUrl", "picasso")
        @JvmStatic
        fun setImageUrl(view: ImageView, imageUrl: String, picasso: Picasso) {
            if(imageUrl.isEmpty()){
                view.visibility = View.GONE
            }else{
                view.visibility = View.VISIBLE
                picasso.load(imageUrl).into(view)
            }
        }
    }
}