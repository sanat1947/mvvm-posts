package net.jet2.mvvmposts.ui.post

import android.util.Log
import androidx.lifecycle.MutableLiveData
import android.view.View
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import net.jet2.mvvmposts.R
import net.jet2.mvvmposts.base.BaseViewModel
import net.jet2.mvvmposts.model.Post
import net.jet2.mvvmposts.model.PostDao
import net.jet2.mvvmposts.network.PostApi
import java.util.Collections.addAll
import javax.inject.Inject

class PostListViewModel(private val postDao: PostDao):BaseViewModel(){
    @Inject
    lateinit var postApi: PostApi
    val postListAdapter: PostListAdapter = PostListAdapter()

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage:MutableLiveData<Int> = MutableLiveData()
    lateinit var postList: List<Post>
    val errorClickListener = View.OnClickListener { loadPosts(1, 10) }
    public var isLoading: Boolean = false

    private lateinit var subscription: Disposable

    init{
        loadPosts(1,10)
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    public fun loadPosts(page:Int, limit:Int){
        subscription = Observable.fromCallable { postDao.all }
                .concatMap {
                    dbPostList ->
                        if(dbPostList.isEmpty()) {
                            Log.d("POst API", "API call start")
                            postApi.getPosts(page,limit).concatMap { apiPostList ->
                                if(apiPostList.size>0) {
                                    if (postList.size > 0) {
                                        with(postList) {
                                            addAll(apiPostList.toMutableList())
                                        }
                                    } else {
                                        postList = apiPostList
                                    }
                                }
                                postDao.insertAll(*postList.toTypedArray())
                                Observable.just(postList)
                            }
                        }
                        else
                            Observable.just(dbPostList)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrievePostListStart() }
                .doOnTerminate { onRetrievePostListFinish() }
                .subscribe(
                        { result -> onRetrievePostListSuccess(result) },
                        { onRetrievePostListError() }
                )
    }

    private fun onRetrievePostListStart(){
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrievePostListFinish(){
        loadingVisibility.value = View.GONE
    }

    private fun onRetrievePostListSuccess(postList:List<Post>){
        isLoading = false
        this.postList = postList
        postListAdapter.updatePostList(this.postList)
    }

    private fun onRetrievePostListError(){
        errorMessage.value = R.string.post_error
    }
}