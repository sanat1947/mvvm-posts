package net.jet2.mvvmposts.model


data class Media(val id: String,
                 val blogId: String,
                 val createdAt: String,
                 val image: String,
                 val title: String,
                 val url: String)