package net.jet2.mvvmposts.model;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class UserTypeConverter implements Serializable {

    @TypeConverter // note this annotation
    public String fromUserList(List<User> userList) {
        if (userList == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<User>>() {
        }.getType();
        String json = gson.toJson(userList, type);
        return json;
    }

    @TypeConverter // note this annotation
    public List<User> toUserList(String userListString) {
        if (userListString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<User>>() {
        }.getType();
        List<User> userList = gson.fromJson(userListString, type);
        return userList;
    }

}
