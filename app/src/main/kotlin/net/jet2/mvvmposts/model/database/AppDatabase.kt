package net.jet2.mvvmposts.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import net.jet2.mvvmposts.model.MediaTypeConverter
import net.jet2.mvvmposts.model.Post
import net.jet2.mvvmposts.model.PostDao
import net.jet2.mvvmposts.model.UserTypeConverter

@Database(entities = [Post::class], version = 1)
@TypeConverters(MediaTypeConverter::class, UserTypeConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun postDao(): PostDao
}