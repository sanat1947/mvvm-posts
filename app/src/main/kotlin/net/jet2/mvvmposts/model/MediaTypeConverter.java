package net.jet2.mvvmposts.model;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class MediaTypeConverter implements Serializable {

    @TypeConverter // note this annotation
    public String fromMediaList(List<Media> mediaList) {
        if (mediaList == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Media>>() {
        }.getType();
        String json = gson.toJson(mediaList, type);
        return json;
    }

    @TypeConverter // note this annotation
    public List<Media> toMediaList(String mediaListString) {
        if (mediaListString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Media>>() {
        }.getType();
        List<Media> mediaList = gson.fromJson(mediaListString, type);
        return mediaList;
    }

}