package net.jet2.mvvmposts.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters

@Entity
data class Post(
        @PrimaryKey
        val id: String,

        val createdAt: String,

        val content: String,

        val comments: Int,

        val likes: Int,

        @TypeConverters(MediaTypeConverter::class)
        val media: List<Media>,

        @TypeConverters(UserTypeConverter::class)
        val user: List<User>
)